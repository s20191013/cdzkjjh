### 安装

1. 克隆项目到本地

2. 从`hbulider`中打开项目

```shell
直接将项目拖进hbulider中点击运行即可
```


### 运行

#### 运行在微信小程序

1.需要保证本地要有`微信开发者工具` https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html

2.在`hbulider`中点击`运行`->`运行到小程序模拟器`->`微信开发者工具` 运行的时候需要配置小程序的`appid`，配置完成后即可运行