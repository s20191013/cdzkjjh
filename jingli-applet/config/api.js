const http = uni.$u.http

// 获取账户信息
export const getAccountInfo=(params) => http.get('/me/accountInfo',params)

// 绑定手机号
export const appletBindMobile=(params) => http.get('/me/appletBindMobile',params)


// 获取充电站列表
export const getPlotInfoPage=(params) => http.get('/charging/getPlotInfoPage',params)
// 获取充电桩信息
export const getChargingPileData=(params) => http.get('/charging/getChargingPileData',params)

// 微信登录
export const appletWeChatLogin=(params) => http.get('/login/appletWeChatLogin',params)

// 保存订单
export const saveOrder=(params) => http.get('/order/saveOrder',params)
